function getAPOD() {
    var div = document.getElementById("apod");
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", "https://api.nasa.gov/planetary/apod?hd=True&api_key=DEMO_KEY", true);
    xmlHttp.responseType = "json";
    xmlHttp.send();
    
    xmlHttp.onload = function(){
        if (xmlHttp.status == 200){
            var resp = xmlHttp.response;
            var title = "<h2>" + resp.title+ "</h2>";
            var img = "<img src=\"" + resp.url + "\" alt=\"failed\" width=\"50%\">";
            var copyright = "<h3>copyright : " + resp.copyright + "</h3>";
            var date = "<h4>date : " + formatDate(new Date(resp.date)) + "</h4>";
            var desc = "<p class=\"ml-5 mr-5\">" + resp.explanation + "</p>"
            div.innerHTML=title+img+copyright+date+desc;
        } else {
            div.innerHTML = "call failed, reason : " + xmlHttp.statusText;
        }
    };
}  

function formatDate(date) {
    var monthNames = [
      "January", "February", "March",
      "April", "May", "June", "July",
      "August", "September", "October",
      "November", "December"
    ];
  
    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();
  
    return day + ' ' + monthNames[monthIndex] + ' ' + year;
  }